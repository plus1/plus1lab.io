import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
export default () => {
    const data = useStaticQuery(graphql`
        query HeroLocQuery {
            matrixLogo: file(relativePath: { eq: "matrix.png" }) {
                childImageSharp {
                    fluid {
                        ...GatsbyImageSharpFluid_tracedSVG
                    }
                }
            }
            loomioLogo: file(relativePath: { eq: "loomio.png" }) {
                childImageSharp {
                    fluid {
                        ...GatsbyImageSharpFluid_tracedSVG
                    }
                }
            }
            telegramLogo: file(relativePath: { eq: "telegram.png" }) {
                childImageSharp {
                    fluid {
                        ...GatsbyImageSharpFluid_tracedSVG
                    }
                }
            }
        }
    `);
    return (
        <div className='herosect'>
            <div className='herotext'>
                <div className='herodesc'>
                    <p className='plusdesc'><span className='letter'>P</span><span className='remletters'>LUS</span> - Promoting
                        Computer user's rights to use,
                        study, copy, modify, and redistribute computer programs.
                    </p>
                    <div className='herojoin'>
                        <p>JOIN US</p>
                        <div className="hero-logos">
                            <a href='https://riot.im/app/#/room/#plus:matrix.org' className="hero-links">
                                <Img
                                    fluid={data.matrixLogo.childImageSharp.fluid}
                                    alt='Matrix Logo'
                                    className='logo-hero matrxi-logo'
                                />
                            </a>
                            <a href='https://codema.in/plus/'  className="hero-links">
                                <Img
                                    fluid={data.loomioLogo.childImageSharp.fluid}
                                    alt='Loomio Logo'
                                    className='logo-hero loomio-logo'
                                />
                            </a>
                            <a href='https://t.me/PlusFreedom'  className="hero-links">
                                <Img
                                    fluid={data.telegramLogo.childImageSharp.fluid}
                                    alt='Loomio Logo'
                                    className='logo-hero telegram-logo'
                                />
                            </a>
                        </div>
                    </div>

                </div>
                <div className='herosun'></div>
            </div>
            <div className='heromountain'><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                <path fill="#00cba9" fill-opacity="1"
                    d="M0,160L17.1,154.7C34.3,149,69,139,103,144C137.1,149,171,171,206,160C240,149,274,107,309,112C342.9,117,377,171,411,213.3C445.7,256,480,288,514,261.3C548.6,235,583,149,617,128C651.4,107,686,149,720,149.3C754.3,149,789,107,823,85.3C857.1,64,891,64,926,53.3C960,43,994,21,1029,26.7C1062.9,32,1097,64,1131,85.3C1165.7,107,1200,117,1234,144C1268.6,171,1303,213,1337,218.7C1371.4,224,1406,192,1423,176L1440,160L1440,320L1422.9,320C1405.7,320,1371,320,1337,320C1302.9,320,1269,320,1234,320C1200,320,1166,320,1131,320C1097.1,320,1063,320,1029,320C994.3,320,960,320,926,320C891.4,320,857,320,823,320C788.6,320,754,320,720,320C685.7,320,651,320,617,320C582.9,320,549,320,514,320C480,320,446,320,411,320C377.1,320,343,320,309,320C274.3,320,240,320,206,320C171.4,320,137,320,103,320C68.6,320,34,320,17,320L0,320Z">
                </path>
            </svg>
            </div>
        </div>
    );
};

